import { string_calc } from '../src/string_calc.js'


describe('Calculadora de strings', () => {
    it('tiene que devolver 0 con un string vacío', () => {
        // Arrange
        const expected_value = 0

        // Act
        const result = string_calc('')

        // Assert
        expect(result).toBe(expected_value)
    })
})
