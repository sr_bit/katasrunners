import unittest
from caesar_cypher import rot13

class TestCaesarCypher(unittest.TestCase):
    def test_a_replaces_single_letter(self):
        self.assertEqual(rot13("A"), "N")

    def test_b_starts_from_the_beginning_if_the_letter_is_at_the_end_of_the_alphabet(self):
        self.assertEqual(rot13("R"), "E")

    def test_c_replaces_a_single_word_with_the_cyphered_one(self):
        self.assertEqual(rot13("FREE"), "SERR")

    def test_d_ignores_spaces_when_cyphering_a_sentence(self):
        self.assertEqual(rot13("FREE PIZZA"), "SERR CVMMN")

    def test_e_ignores_non_alphanumeric_characters(self):
        self.assertEqual(rot13("FREE PIZZA?!"), "SERR CVMMN?!")
