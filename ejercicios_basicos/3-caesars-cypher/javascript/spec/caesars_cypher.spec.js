import { rot13 } from '../src/caesars_cypher.js'

describe('The Caesar cypher', () => {
    it ('Cyphers a single character to the cyphered character', () => {
        // Arrange
        const starting_character = 'A'
        const cyphered_character = 'N'

        // Act
        const result = rot13(starting_character)

        // Assert
        expect(result).toBe(cyphered_character)
    })

    it ('Starts from the beginning if the character that has to be replaced is at the end of the alphabet', () => {
        // Arrange
        const starting_character = 'R'
        const cyphered_character = 'E'

        // Act
        const result = rot13(starting_character)

        // Assert
        expect(result).toBe(cyphered_character)
    })

    it ('Cyphers a single word to the cyphered word', () => {
        // Arrange
        const starting_word = 'FREE'
        const cyphered_word = 'SERR'

        // Act
        const result = rot13(starting_word)

        // Assert
        expect(result).toBe(cyphered_word)
    })

    it ('Ignores spaces when cyphering a sentence', () => {
        // Arrange
        const starting_word = 'FREE PIZZA'
        const cyphered_word = 'SERR CVMMN'

        // Act
        const result = rot13(starting_word)

        // Assert
        expect(result).toBe(cyphered_word)
    })

    it ('Ignores any non alphabetic character when cyphering a sentence', () => {
        // Arrange
        const starting_word = 'FREE PIZZA?!'
        const cyphered_word = 'SERR CVMMN?!'

        // Act
        const result = rot13(starting_word)

        // Assert
        expect(result).toBe(cyphered_word)
    })
})
