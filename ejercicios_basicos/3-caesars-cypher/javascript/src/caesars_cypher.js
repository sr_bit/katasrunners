export function rot13(str) { // LBH QVQ VG!

  var listChars = str.split("")
    var strAscii
    var newString = ''
    var newAscii
    var char
    const codify = 13

    for (var i = 0; i < listChars.length; i++){
    
      char = listChars[i]
      
      strAscii = char.charCodeAt()

      newAscii=strAscii
  
      if (strAscii > 64 && strAscii < 77){
        newAscii = strAscii+codify
      }
    
      if (strAscii > 76 && strAscii < 91){
        newAscii = 64+(codify-(90 - strAscii))
      }

      newString = newString + String.fromCharCode(newAscii)
  
    }
     
  return newString

}
