export function palindrome(aString) {
  var character = ""
  var charAscii
  var soloLetras = ""

  var lowerCaseString = aString.toLowerCase()

  for (var i = 0;(i<lowerCaseString.length); i++){
    character = lowerCaseString.charAt(i)
    charAscii = character.charCodeAt(0)
    if (charAscii >= 97 && charAscii <= 122 || charAscii == 241){
      soloLetras = soloLetras + character
    }
  }
 
  var splitString = soloLetras.split("")
  var reversedArray = splitString.reverse()
  var joinString = reversedArray.join("")
    
  if (joinString === soloLetras){
    return true
  }
  return false
}
