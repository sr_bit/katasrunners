export function convertToRoman(num) {
    var numUser = num
    var stringToReturn = ''
    var txt = ''

    var unidades
    var decenas
    var centenas
    var unidadesMillar
    var decenasMillar

    decenasMillar=Math.trunc(numUser/1000)
    numUser=numUser%1000
    txt = millaresFunc(decenasMillar)
    stringToReturn = stringToReturn + txt

    
    centenas=Math.trunc(numUser/100)
    numUser=numUser%100
    txt = centenaresFunc(centenas)
    stringToReturn = stringToReturn + txt
    
    decenas=Math.trunc(numUser/10)
    numUser=numUser%10
    txt = decenasFunc(decenas)
    stringToReturn = stringToReturn + txt

    txt = unidadesFunc(numUser)
    stringToReturn = stringToReturn + txt
    
    return stringToReturn
}

function millaresFunc (numMillares){
    var miles = ["","M","MM","MMM"]
    return miles[numMillares]
}

function centenaresFunc (numCentenares){
    var cientos = ["","C","CC","CCC","CD","D",
                    "DC","DCC","DCC","CM"]
    return cientos[numCentenares]
}

function decenasFunc (numDecenas){
    var lasDecenas = ["","X","XX","XXX","XL","L",
                        "LX","LXX","LXXX","XC"]
    return lasDecenas[numDecenas]
}

function unidadesFunc (numSingles){
    var lasUnidades = ["","I","II","III","IV", "V",
    "VI", "VII", "VIII", "IX", "X"]
    return lasUnidades[numSingles]
}   
