import { checkCashRegister } from '../src/cash_register'

describe('A cash register', () => {
    const cashInDrawer = [
        ["PENNY", 1.01],
        ["NICKEL", 2.05],
        ["DIME", 3.1],
        ["QUARTER", 4.25],
        ["ONE", 90],
        ["FIVE", 55],
        ["TEN", 20],
        ["TWENTY", 60],
        ["ONE HUNDRED", 100]
    ]

    const ONE_PENNY_DRAWER = [["PENNY", 0.01]]
    const TWO_PENNIES_DRAWER = [["PENNY", 0.02]]
    const THREE_PENNIES_DRAWER = [["PENNY", 0.03]]
    const TWO_COINS_IN_DRAWER = [["PENNY", 0.23], ["NICKEL", 0.15]]
    const ALL_COINS_DRAWER = [["PENNY", 1.01], ["NICKEL", 2.05], ["DIME", 3.1], ["QUARTER", 4.25], ["ONE", 90], ["FIVE", 55], ["TEN", 20], ["TWENTY", 60], ["ONE_HUNDRED", 100]]

    it ("Returns insufficient funds when there is not enough change in drawer", () => {
        // Arrange
        const expected_result = { status: "INSUFFICIENT_FUNDS", change: []}
        const order_price = 0.01
        const given_cash = 0.03

        // Act
        const change = checkCashRegister(order_price, given_cash, ONE_PENNY_DRAWER)

        // Assert
        expect(change).toEqual(expected_result)
    })

    it ('Closes the drawer when the change is the same amount as the cash in drawer', () => {
        // Arrange
        const expected_result = { status: "CLOSED", change: [['PENNY', 0.02]]}
        const order_price = 0.01
        const given_cash = 0.03

        // Act
        const change = checkCashRegister(order_price, given_cash, TWO_PENNIES_DRAWER)

        // Assert
        expect(change).toEqual(expected_result)
    })

    it ("Returns no change when the given cash is the same as the price", () => {
        // Arrange
        const expected_result = { status: "OPEN", change: []}

        // Act
        const change = checkCashRegister(5, 5, ONE_PENNY_DRAWER)

        // Assert
        expect(change).toEqual(expected_result)
    })

    it ('Returns a single penny when the change is one penny', () => {
        // Arrange
        const expected_result = { status: "OPEN", change: ONE_PENNY_DRAWER}
        const order_price = 0.01
        const given_cash = 0.02

        // Act
        const change = checkCashRegister(order_price, given_cash, TWO_PENNIES_DRAWER)

        // Assert
        expect(change).toEqual(expected_result)
    })

    it ('Returns the change when drawer has a single coin.', () => {
        // Arrange
        const expected_result = { status: "OPEN", change: TWO_PENNIES_DRAWER}
        const order_price = 0.01
        const given_cash = 0.03

        // Act
        const change = checkCashRegister(order_price, given_cash, THREE_PENNIES_DRAWER)

        // Assert
        expect(change).toEqual(expected_result)
    })

    it ('Handles returning change when having two coins in the drawer and returning a single coin type.', () => {
        // Arrange
        const expected_result = { status: "OPEN", change: TWO_PENNIES_DRAWER}
        const order_price = 0.01
        const given_cash = 0.03

        // Act
        const change = checkCashRegister(order_price, given_cash, TWO_COINS_IN_DRAWER)

        // Assert
        expect(change).toEqual(expected_result)
    })

    it ('Handles returning change when having two coins in the drawer and returning two coin types.', () => {
        // Arrange
        const expected_result = { status: "OPEN", change: [["NICKEL", 0.10], ["PENNY", 0.02]]}
        const order_price = 0.01
        const given_cash = 0.13

        // Act
        const change = checkCashRegister(order_price, given_cash, TWO_COINS_IN_DRAWER)

        // Assert
        expect(change).toEqual(expected_result)
    })

    it ('Handles returning the right change independently of the types of the coins.', () => {
        // Arrange
        const expected_change = [['TWENTY', 60], ['TEN', 20], ['FIVE', 15], ['ONE', 1], ['QUARTER', 0.5], ['DIME', 0.2], ['PENNY', 0.04]]
        const expected_result = { status: "OPEN", change: expected_change }
        const order_price = 3.26
        const given_cash = 100

        // Act
        const change = checkCashRegister(order_price, given_cash, ALL_COINS_DRAWER)

        // Assert
        expect(change).toEqual(expected_result)
    })
})
