import { fizzbuzz } from "../src/fizzbuzz"

describe('Fizzbuzz', () => {
    it('should return fizz if the number is divisible by 3', () => {
        // Arrange
        const expected_value = "Fizz"

        // Act
        const result = fizzbuzz(3)

        // Assert
        expect(result).toBe(expected_value)
    })
    it('should return buzz if the number is divisible by 5', () => {
         // Arrange
         const expected_value = "Buzz"

         // Act
         const result = fizzbuzz(5)
 
         // Assert
         expect(result).toBe(expected_value)
    })
    it('should return FizzBuzz if the number is divisible by both', () => {
        // Arrange
        const expected_value = "FizzBuzz"

        // Act
        const result = fizzbuzz(15)

        // Assert
        expect(result).toBe(expected_value)
    })
    it('should return the number as a string if the number is not divisible by 3 nor 5', () => {
        // Arrange
        const expected_value = "1"

        // Act
        const result = fizzbuzz(1)

        // Assert
        expect(result).toBe(expected_value)
    })
})  