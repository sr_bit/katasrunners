export function fizzbuzz(variable) {

    let FIZZ = "Fizz"
    let BUZZ = "Buzz"

    if (isFizz(variable) && isBuzz(variable)){
        return FIZZ+BUZZ;
    } else if (isFizz(variable)){
        return FIZZ;
    } else if (isBuzz(variable)){
        return BUZZ;
    } else {
        return variable.toString();
    }
}

function isFizz(variable){
    return variable % 3 == 0 
}    
function isBuzz(variable){
    return variable % 5 == 0 
}