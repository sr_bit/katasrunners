export function stringcalculator(line) {
     if (mayor_que_uno(line)) {
        return  suma(line);
    } 
    return Number(line);
}

function mayor_que_uno(line) {
    return line.length > 1
} 

function suma(line){
    let total = 0
    let numbers = convert_string_to_array(line);
    for (var i = 0; i < numbers.length; i++){
        total += numbers[i]
    }
    return total
}
function convert_string_to_array(line){
    return line.split(",").map(Number)
}
